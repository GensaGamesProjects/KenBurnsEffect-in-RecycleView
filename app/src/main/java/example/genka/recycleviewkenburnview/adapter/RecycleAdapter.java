package example.genka.recycleviewkenburnview.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import example.genka.recycleviewkenburnview.R;
import example.genka.recycleviewkenburnview.model.PersonCard;
import example.genka.recycleviewkenburnview.view.KenBurnsView;

/**
 * Created by Genka on 06.10.2015.
 */
public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.MainHolder> {

    private List<PersonCard> listPerson;
    private Context context;
    MyClickListener myClickListener;

    public RecycleAdapter ( List<PersonCard> _listPerson, Context _context) {
        listPerson = _listPerson;
        context = _context;
    }

    @Override
    public MainHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_main_item, parent, false);
        MainHolder viewHolder = new MainHolder(v);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final MainHolder holder, int position) {
        PersonCard currentPerson = listPerson.get(position);
        holder.textTitle.setGravity(position % 2 == 0 ? Gravity.RIGHT : Gravity.LEFT);
        holder.textTitle.setText(currentPerson.getTitle());
        holder.textDescrip.setText(currentPerson.getShortDescrip());
        Picasso.with(context)
                .load(currentPerson.getImageUrl())
                .error(R.drawable.nature_photo5)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        holder.image.setResourceIds(bitmap, bitmap); }
                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {}
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {}
                });
    }

    @Override
    public int getItemCount() {
        return listPerson.size();
    }



    public void setOnItemClickListener(MyClickListener _myClickListener) {
        myClickListener = _myClickListener;
    }


    public void clear() {
        listPerson.clear();
        notifyDataSetChanged();
    }


    public class MainHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        TextView textTitle;
        TextView textDescrip;
        //ImageView image;
        KenBurnsView image;


        public MainHolder(View itemView) {
            super(itemView);
            textTitle = (TextView) itemView.findViewById(R.id.title_text);
            textDescrip = (TextView) itemView.findViewById(R.id.descip_text);
            //image = (ImageView) itemView.findViewById(R.id.image_title);
            image = (KenBurnsView) itemView.findViewById(R.id.image_title);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public interface MyClickListener {
        void onItemClick(int position, View v);
    }

}
